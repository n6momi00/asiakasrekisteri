<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <title>Asiakasrekisteri 1</title>
    </head>
    <body>
        <h3>Asiakkaat</h3>
        <p><a href="asiakas.php">Lisää uusi</a></p>
        <?php
        try {
            $tietokanta = new PDO('mysql:host=localhost;dbname=asiakasrekisteri;charset=utf8','root','');
            
            $tietokanta ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            $sql = 'SELECT * FROM asiakas ORDER BY sukunimi';
            
            $kysely = $tietokanta ->query($sql);
            
            if ($kysely) {
               print '<table cellspacing="0">';
               print '<tr>';
               print '<th>Sukunimi</th>';
               print '<th>Etunimi</th>';
               print '<th>Email</th>';
               print '<th></th>';
               print '<th></th>';
               print '<th></th>';
               print '</tr>';
               
               while ($tietue = $kysely ->fetch()) {
                   print '<tr>';
                   print '<td>' . $tietue['sukunimi'] . '</td>';
                   print '<td>' . $tietue['etunimi'] . '</td>';
                   print '<td>' . $tietue['email'] . '</td>';
                   print '<td> <a href="asiakas.php?id='. $tietue['id'] . '">Muokkaa</a></td>';
                   print '<td> <a href="poista.php?id='. $tietue['id'] . '" onclick="return confirm(\'Jatketaanko?\');">Poista</a></td>';
                   print '<td> <a href="muistiinpanot.php?id='. $tietue['id'] . '">Muistiinpanot</a></td>';
                   print '</tr>';
               }
               print '</table>';
            } else {
                print '<p>';
                print_r($tietokanta -> errorInfo());
                print'</p>';
            }
        } catch (PDOException $pdoex) {
            print '<p>Tietokannan avaus epäonnistui.' . $pdoex -> getMessage() .'</p>';
        }
        ?>        
    </body>
</html>
