<?php
$otsikko = 'Lisää asiakas';
$id = 0;
$etunimi = '';
$sukunimi = '';
$email = '';

$tietokanta = new PDO('mysql:host=localhost;dbname=asiakasrekisteri;charset=utf8','root','');
            
$tietokanta ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    if (isset($_GET['id'])) {
        $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
            
        try {
        
            $sql = "SELECT * FROM asiakas WHERE ID=$id";
                
            $kysely = $tietokanta ->query($sql);
               
            if($kysely) {
                $tietue = $kysely ->fetch();
                $etunimi = $tietue['etunimi'];
                $sukunimi = $tietue['sukunimi'];
                $email = $tietue['email'];
                $otsikko = "Muokkaa asiakkaan $sukunimi, $etunimi tietoja";
            } else {
                print '<p>';
                print_r($tietokanta -> errorInfo());
                print '</p>';
            }
            print("<a href='index.php'>Etusivulle</a>");
        } catch (PDOException $pdoex) {
            print '<p>Tietokannan avaus epäonnistui.' . $pdoex -> getMessage() . '</p>';
        }       
    }
} else if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    try {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
        $sukunimi = filter_input(INPUT_POST, 'sukunimi', FILTER_SANITIZE_STRING);
        $etunimi = filter_input(INPUT_POST, 'etunimi', FILTER_SANITIZE_STRING);
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
        
        if ($id == 0) {
            $kysely = $tietokanta -> prepare("INSERT INTO asiakas(sukunimi, etunimi, email) VALUES (:sukunimi, :etunimi, :email)");
        } else {
            $kysely = $tietokanta -> prepare("UPDATE asiakas SET sukunimi = :sukunimi, etunimi = :etunimi, email = :email WHERE id = :id");
            $kysely -> bindValue(':id', $id, PDO::PARAM_INT);
        }
        
        $kysely -> bindValue(':sukunimi', $sukunimi, PDO::PARAM_INT);
        $kysely -> bindValue(':etunimi', $etunimi, PDO::PARAM_INT);
        $kysely -> bindValue(':email', $email, PDO::PARAM_INT);        
        
        if($kysely ->execute()) {
            print('<p>Asiakkaan tiedot tallenenttu</p>');
            $id = $tietokanta ->lastInsertId();
        } else {
            print '<p>';
            print_r($tietokanta -> errorInfo());
            print '</p>';
        }
    } catch (PDOException $pdoex) {
            print '<p>Tietokannan avaus epäonnistui.' . $pdoex -> getMessage() . '</p>';
    }
}

?>
<h3><?php echo $otsikko; ?></h3>
        
<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
    <input type="hidden" name="id" value="<?php print($id); ?>">
    <p>Sukunimi:<br>
    <input type="text" name="sukunimi" maxlength="30" value="<?php print($etunimi); ?>" required></p>
    
    <p>Etunimi:<br>
    <input type="text" name="etunimi" maxlength="30" value="<?php print($sukunimi); ?>" required></p>

    <p>Sähköposti:<br>
    <input type="email" name="email" maxlength="100" value="<?php print($email); ?>" required></p>

    <p><button>Tallenna</button>&nbsp;
    <input type="reset" value="Tyhjennä">&nbsp;
    <button type="button" onclick="window.location='index.php';">Peruuta</button></p>
</form>
