<body>
<h3>Muistiinpanot</h3>
<a href="index.php">Etusivulle</a>

<?php
$id = 0;
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
} else if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    try {
        $tietokanta = new PDO('mysql:host=localhost;dbname=asiakasrekisteri;charset=utf8','root','');
            
        $tietokanta ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
        $teksti = filter_input(INPUT_POST, 'teksti', FILTER_SANITIZE_STRING);
    
        $kysely = $tietokanta ->prepare("INSERT INTO muistiinpano(teksti, asiakas_id) VALUES (:teksti, :asiakas_id)");

        $kysely ->bindValue(':asiakas_id', $id, PDO::PARAM_INT);
        $kysely ->bindValue(':teksti', $teksti, PDO::PARAM_STR);
    
        $kysely ->execute();
        print('<p>Muistiinpano tallennettu!</p>');
    } catch (PDOException $pdoex) {
        print '<p>Tietokannan avaus epäonnistui.' . $pdoex -> getMessage() . '</p>';
    }
}
?>
<form action="<?php print $_SERVER['PHP_SELF']; ?>" method="post">
    <textarea name="teksti"></textarea>
    <br> <button>Lisää</button>
    <input type="hidden" name="id" value="<?php print($id);?>">
</form>
<?php
try {
    $tietokanta = new PDO('mysql:host=localhost;dbname=asiakasrekisteri;charset=utf8','root','');
            
    $tietokanta ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sql = "SELECT * FROM muistiinpano WHERE asiakas_id = $id ORDER BY aika DESC";
    
    $kysely = $tietokanta -> query($sql);
    
    print '<div>';
    
    while ($tietue = $kysely -> fetch()) {
        print '<p>';
        print '<span>' . date('d.m.Y H.i', strtotime($tietue['aika'])) . '</span><br>';
        print $tietue['teksti'];
        print '</p>';
    }
    print '</div>';
    
} catch (PDOException $pdoex) {
    print '<p>Tietokannan avaus epäonnistui.' . $pdoex -> getMessage() .'</p>';
}
?>
</body>