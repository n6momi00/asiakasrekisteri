/* 
    Created on : 2.10.2016, 11:02:14
    Author     : Miika
*/

drop database if exists asiakasrekisteri;

create database asiakasrekisteri;

use asiakasrekisteri;

create table asiakas(
    id int primary key auto_increment,
    etunimi varchar(30) not null,
    sukunimi varchar(30) not null,
    email varchar(100) unique,
    puhelin varchar(15)
);

create table muistiinpano(
    id int primary key auto_increment,
    teksti text not null,
    asiakas_id int not null,
    foreing key (asiakas_id) references asiakas(id) on delete restrict,
    aika timestamp default current_timestamp on update current_timestamp
);

insert into asikas (etunimi,sukunimi,email) values ('Jouni','Juntunen','jouni.juntunen@oamk.fi');
insert into muistiinpano (teksti,asiakas_id) values ('Testiä',1);