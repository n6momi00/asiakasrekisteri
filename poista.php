<h3>Poista asiakas</h3>
<?php
$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);

try {

$tietokanta = new PDO('mysql:host=localhost;dbname=asiakasrekisteri;charset=utf8','root','');
            
$tietokanta ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$kysely = $tietokanta ->prepare("DELETE FROM asiakas WHERE id=:id");

$kysely -> bindValue(':id', $id, PDO::PARAM_INT);

if($kysely -> execute()) {
    print ('<p>Asiakkaan tiedostot poistettu!</p>');
} else {
    print '<p>';
    print_r($tietokanta -> errorInfo());
    print '</p>';
}

print("<a href='index.php'>Etusivulle</a>");

} catch (PDOException $pdoex) {
        print '<p>Tietokannan avaus epäonnistui.' . $pdoex -> getMessage() . '</p>';
    }